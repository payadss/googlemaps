package th.ac.tu.siit.lab9googlemaps;

import java.util.Random;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.R.color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {
	
	int CC = Color.RED;
	GoogleMap map;
	LocationManager locManager;
	Location currentLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		
		setContentView(R.layout.activity_main);
		MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		
		map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		map.setMyLocationEnabled(true);
		
		/*MarkerOptions mo = new MarkerOptions();
		mo.position(new LatLng(0,0));
		mo.title("Hello");
		
		map.addMarker(mo);
		
		PolylineOptions po = new PolylineOptions();
		po.add(new LatLng (0,0));
		po.add(new LatLng(13.75,100.4667));
		po.width(5);
		
		map.addPolyline(po);*/
		
		
		
		//map.setMyLocationEnabled(true);
		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		
		LocationListener locListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location l) {
				
				PolylineOptions po1 = new PolylineOptions();
				po1.add(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
				po1.add(new LatLng(l.getLatitude(),l.getLongitude()));
				po1.width(3);
				po1.color(CC);
				map.addPolyline(po1);
				
				currentLocation = l;
				
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};
		
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 4, locListener);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public int random(){
		Random r = new Random();
		float[] hsv = new float[3];
		hsv[1] = 1.0f;
		hsv[2] = 1.0f;
		hsv[0] = r.nextInt(360);
		int c = Color.HSVToColor(hsv);
		return c;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
			switch(item.getItemId()) {
			case R.id.Mark:
				MarkerOptions mo = new MarkerOptions();
				mo.position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
				mo.icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.ic_launcher));
                       // .draggable(true));
				map.addMarker(mo);
				break;
			case R.id.Color:
				CC = random();
				break;
			}				
			
		return super.onOptionsItemSelected(item);
	}
}
